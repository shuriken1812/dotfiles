function fish_greeting
    echo """
⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣠⣤⣤⣤⣄⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⢀⣾⣿⣿⣿⣯⣽⣿⣿⣦⣀⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⣾⣿⣿⣿⡿⢿⡟⢿⣽⣿⣿⡣⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⢰⢿⣯⠁⠀⠀⠸⣷⣌⣿⣿⣿⡟⠄⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠘⣾⣾⣶⣄⠰⠟⠻⠆⠘⣧⡟⠇⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⢫⠉⠉⡀⠀⠀⠀⢠⣿⣿⡂⠀⠀⠀⠀⠀⠀⠀⠀⠀ Loose & you die. Win & you live.
⠀⠀⠀⠀⠀⠀⠀⠀⠀⢣⡀⢘⡂⠄⢀⡾⡏⡇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀ You can't win if you don't fight.
⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⢦⣠⠔⣻⢱⠁⢳⣤⣤⣀⡀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⢀⣠⣶⡿⠹⠾⠁⣸⢀⣈⣻⣧⠉⢻⣦⡀⠀⠀⠀⠀ 勝てなきゃ死ぬ。勝てば生きる。
⠀⠀⠀⠀⠀⠀⣠⣾⢿⣽⠯⢶⡇⠀⣠⣗⣛⠶⢾⡇⢰⣿⡣⣿⣆    戦わなければ勝てない。
⠀⠀⠀⠀⠀⣰⣿⣿⡻⣿⡉⢱⣤⡼⢯⡨⠃⣰⣿⣵⠟⣁⡴⠋⠁⣷⡄⠀
⠀⠀⠀⠀⢸⣿⢻⣎⢷⡇⢣⡏⣿⠭⢹⠀⢰⣿⢟⣵⠾⠋⠀⠀⢠⠟⣽⠀
⠀⠀⠀⠀⣿⣿⠀⠹⣾⣷⡀⠳⠦⠒⠉⢀⣾⣿⣿⠁⢠⣆⠀⠀⡎⠀⢸⡄
⠀⠀⠀⢸⣿⣿⠀⠀⣿⠀⢳⡀⠀⣀⠴⡋⣿⣿⡁⠀⢈⣿⠀⣸⡃⠀⢽⡃
⠀⠀⠀⢸⡟⣿⠀⠀⡿⠀⠀⠱⠀⠀⠀⠀⣿⡟⠁⠀⢻⣽⡆⠋⡇⠀⢸⡇⠀
"""
end
starship init fish | source

set EDITOR "emacsclient -t -a ''"         # $EDITOR use Emacs in terminal
set VISUAL "emacsclient -nc"              # $VISUAL use Emacs in GUI mode

alias emx="emacsclient -nc"
fish_add_path $HOME/.emacs.d/bin
